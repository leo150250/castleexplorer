const fase=document.getElementById("fase");
const jogador=document.getElementById("jogador");
const mira=document.getElementById("mira");
function gerarGrade(argMapX,argMapY) {
	var gradeId="gx"+argMapX+"y"+argMapY;
	Math.seedrandom(gradeId);
	var novaGrade=document.createElement("div");
	novaGrade.id=gradeId;
	novaGrade.classList.add("grid");
	novaGrade.style.position="absolute";
	novaGrade.style.left=argMapX*320;
	novaGrade.style.top=argMapY*320;
	for (var i=0; i<10; i++) {
		for (var j=0; j<10; j++) {
			var novoBloco=document.createElement("div");
			novoBloco.id=gradeId+"b"+(((i*10)+1)+j);
			novoBloco.classList.add("bloco");
			novaGrade.appendChild(novoBloco);
		}
	}
	var cursor=(Math.floor(Math.random()*100)+1);
	//console.log(cursor);
	novaGrade.children[cursor-1].classList.add("destaque");
	var iteracoes=50;
	for (var i=0; i<iteracoes; i++) {
		var direcao=(Math.floor(Math.random()*4))*90;
		var distancia=Math.floor(Math.random()*5+1);
		//console.log(distancia+" em "+direcao);
		for (var j=0; j<distancia; j++) {
			cursor=moverGerador(cursor,direcao);
			novaGrade.children[cursor-1].classList.add("destaque");
		}
	}
	for (var i=0; i<100; i++) {
		if (!novaGrade.children[i].classList.contains("destaque")) {
			for (var j=1; j<=4; j++) {
				var criarParede=true;
				switch (j) {
					case 1: {
						var paredeVizinha=novaGrade.children[i-10];
						if (paredeVizinha==undefined) {
							criarParede=true;
						} else {
							if (paredeVizinha.classList.contains("destaque")) {
								criarParede=true;
							} else {
								criarParede=false;
							}
						}
					} break;
					case 2: {
						var paredeVizinha=novaGrade.children[i+1];
						if (paredeVizinha==undefined || i%10==9) {
							criarParede=true;
						} else {
							if (paredeVizinha.classList.contains("destaque")) {
								criarParede=true;
							} else {
								criarParede=false;
							}
						}
					} break;
					case 3: {
						var paredeVizinha=novaGrade.children[i+10];
						if (paredeVizinha==undefined) {
							criarParede=true;
						} else {
							if (paredeVizinha.classList.contains("destaque")) {
								criarParede=true;
							} else {
								criarParede=false;
							}
						}
					} break;
					case 4: {
						var paredeVizinha=novaGrade.children[i-1];
						if (paredeVizinha==undefined || i%10==0) {
							criarParede=true;
						} else {
							if (paredeVizinha.classList.contains("destaque")) {
								criarParede=true;
							} else {
								criarParede=false;
							}
						}
					} break;
				}
				if (criarParede) {
					var novaParede=document.createElement("div");
					novaParede.classList.add("parede");
					novaParede.classList.add("p"+j);
					if (j==1 || j==3) {
						novaParede.style.backgroundImage="url(\"parede1a.png\")";
					} else if (j==2 || j==4) {
						novaParede.style.backgroundImage="url(\"parede1b.png\")";
					}
					novaGrade.children[i].appendChild(novaParede);
				}
			}
		}
	}
	fase.appendChild(novaGrade);
	console.log("Grade "+gradeId+" gerada");
}
function limparGrade(argMapX,argMapY) {
	var gradeLimpar=document.getElementById("gx"+argMapX+"y"+argMapY);
	gradeLimpar.parentNode.removeChild(gradeLimpar);
}
function moverGerador(argCursor,argDirecao) {
	switch (argDirecao) {
		case 0: {
			if (argCursor%10>0) {
				argCursor++;
				//console.log("Moveu para direita - "+argCursor);
			} else {
				//console.log("Movimento cancelado. Já está no talo!");
			}
		} break;
		case 90: {
			if (argCursor>10) {
				argCursor-=10;
				//console.log("Moveu para cima - "+argCursor);
			} else {
				//console.log("Movimento cancelado. Já está no talo!");
			}
		} break;
		case 180: {
			if (argCursor%10!=1) {
				argCursor--;
				//console.log("Moveu para esquerda - "+argCursor);
			} else {
				//console.log("Movimento cancelado. Já está no talo!");
			}
		} break;
		case 270: {
			if (argCursor<90) {
				argCursor+=10;
				//console.log("Moveu para baixo - "+argCursor);
			} else {
				//console.log("Movimento cancelado. Já está no talo!");
			}
		} break;
	}
	return argCursor;
}
for (var y=-1; y<=1; y++) {
	for (var x=-1; x<=1; x++) {
		gerarGrade(x,y);
	}
}
var posicaoX=160;
var posicaoY=160;
//var telaZ=((-window.innerHeight/2)-16);
var direcaoTela=90;
var respiracaoFX=0.25;
var respiracaoFXvelocidade=0;
function respiracaoFXupdate() {
	if (respiracaoFX<0) {
		respiracaoFXvelocidade+=0.01;
	} else if (respiracaoFX>0) {
		respiracaoFXvelocidade-=0.01;
	}
	if (respiracaoFXvelocidade>0.05) {
		respiracaoFXvelocidade=0.05;
	} else if (respiracaoFXvelocidade<-0.05) {
		respiracaoFXvelocidade=-0.05;
	}
}
setInterval(respiracaoFXupdate,200);
var posicaoGradeX=Math.floor(posicaoX/320);
var posicaoGradeY=Math.floor(posicaoY/320);
function atualizarJogo() {
	respiracaoFX+=respiracaoFXvelocidade;
	if (respiracaoFX>0.25) {
		respiracaoFX=0.25;
	} else if (respiracaoFX<-0.25) {
		respiracaoFX=-0.25;
	}
	if (virar==1) {
		direcaoTela+=2.5;
	} else if (virar==-1) {
		direcaoTela-=2;
	}
	if (direcaoTela>=360) {
		direcaoTela-=360;
	}
	if (direcaoTela<0) {
		direcaoTela+=360;
	}
	var previsaoX=8*Math.cos((direcaoTela*-1)*Math.PI/180);
	var previsaoY=8*Math.sin((direcaoTela*-1)*Math.PI/180);
	if (andar==1) {
		var idBlocoPrevisto=(Math.floor((posicaoX+previsaoX)/32)+1-(Math.floor((posicaoX+previsaoX)/320)*10))+((Math.floor((posicaoY+previsaoY)/32)+1-(Math.floor((posicaoY+previsaoY)/320)*10))*10-10);
		var blocoPrevisto=document.getElementById("gx"+Math.floor((posicaoX+previsaoX)/320)+"y"+Math.floor((posicaoY+previsaoY)/320)+"b"+idBlocoPrevisto);
		if (!blocoPrevisto.classList.contains("destaque")) {
			//console.log(blocoPrevisto.offsetTop+" - "+(Math.floor(posicaoY/32)*32));
			//Essa colisão tá toda cagada. HELP, I NEED SOMEBOOOODY!
			/*
			var zerarPrevisaoX=false;
			var zerarPrevisaoY=false;
			if (direcaoTela<45 || direcaoTela>=315) {
				zerarPrevisaoX=true;
			}
			if (direcaoTela<135 && direcaoTela>=45) {
				zerarPrevisaoY=true;
			}
			if (direcaoTela<225 && direcaoTela>=135) {
				zerarPrevisaoX=true;
			}
			if (direcaoTela<315 && direcaoTela>=225) {
				zerarPrevisaoY=true;
			}
			/*if (blocoPrevisto.offsetTop==Math.floor((posicaoY+previsaoY)/32)*32) {
				zerarPrevisaoX=true;
			}
			if (zerarPrevisaoX) {
				previsaoX=0;
			}
			if (zerarPrevisaoY) {
				previsaoY=0;
			}*/
			//Código extremamente confiável que nunca vai falhar. Espero.
			previsaoX=0;
			previsaoY=0;
		}
		posicaoX+=previsaoX/4;
		posicaoY+=previsaoY/4;
	} else if (andar==-1) {
		posicaoX-=previsaoX/4;
		posicaoY-=previsaoY/4;
	}
	atualizarMapeamento();
	//console.log((Math.floor(posicaoX/32)+1-(posicaoGradeX*10))+","+((Math.floor(posicaoY/32)+1-(posicaoGradeY))*10-10)+" - "+((Math.floor(posicaoX/32)+1-(posicaoGradeX*10))+((Math.floor(posicaoY/32)+1-(posicaoGradeY*10))*10-10)));
	jogador.style.transform="rotateZ("+(direcaoTela*-1)+"deg)";
	jogador.style.left=posicaoX-16+"px";
	jogador.style.top=posicaoY-16+"px";
	mira.style.left=posicaoX-8+(32*Math.cos((direcaoTela*-1)*Math.PI/180));
	mira.style.top=posicaoY-8+(32*Math.sin((direcaoTela*-1)*Math.PI/180));
	atualizarTela();
}
var topDown=false;
function atualizarTela() {
	if (!topDown) {
		telaZ=-18+respiracaoFX;
		telaDistancia=window.innerWidth/2;
		telaX=(window.innerWidth/2)-posicaoX-1.5+(-telaDistancia*Math.cos((direcaoTela*-1)*Math.PI/180));
		telaY=(window.innerHeight/2)-posicaoY-0+(-telaDistancia*Math.sin((direcaoTela*-1)*Math.PI/180));
		tela.style.perspective=telaDistancia+"px";
		fase.style.transformOrigin=window.innerWidth/2+"px "+window.innerHeight/2+"px";
		fase.style.transform="rotateX(90deg) rotateZ("+(direcaoTela-90)+"deg) translateX("+telaX+"px) translateY("+telaY+"px) translateZ("+telaZ+"px)";
	} else {
		telaZ=0;
		telaDistancia=window.innerWidth;
		telaX=(window.innerWidth/2)-posicaoX;
		telaY=(window.innerHeight/2)-posicaoY;
		tela.style.perspective=telaDistancia+"px";
		fase.style.transformOrigin=window.innerWidth/2+"px "+window.innerHeight/2+"px";
		fase.style.transform="translateX("+telaX+"px) translateY("+telaY+"px) translateZ("+telaZ+"px)";
	}
}
function atualizarMapeamento() {
	if (Math.floor(posicaoX/320)!=posicaoGradeX) {
		if (posicaoGradeX<Math.floor(posicaoX/320)) {
			console.log("Indo pra direita...");
			gerarGrade(posicaoGradeX+2,posicaoGradeY-1);
			gerarGrade(posicaoGradeX+2,posicaoGradeY);
			gerarGrade(posicaoGradeX+2,posicaoGradeY+1);
			limparGrade(posicaoGradeX-1,posicaoGradeY-1);
			limparGrade(posicaoGradeX-1,posicaoGradeY);
			limparGrade(posicaoGradeX-1,posicaoGradeY+1);
		} else if (posicaoGradeX>Math.floor(posicaoX/320)) {
			console.log("Indo pra esquerda...");
			gerarGrade(posicaoGradeX-2,posicaoGradeY-1);
			gerarGrade(posicaoGradeX-2,posicaoGradeY);
			gerarGrade(posicaoGradeX-2,posicaoGradeY+1);
			limparGrade(posicaoGradeX+1,posicaoGradeY-1);
			limparGrade(posicaoGradeX+1,posicaoGradeY);
			limparGrade(posicaoGradeX+1,posicaoGradeY+1);
		}
	}
	if (Math.floor(posicaoY/320)!=posicaoGradeY) {
		if (posicaoGradeY>Math.floor(posicaoY/320)) {
			console.log("Indo pra cima...");
			gerarGrade(posicaoGradeX-1,posicaoGradeY-2);
			gerarGrade(posicaoGradeX,posicaoGradeY-2);
			gerarGrade(posicaoGradeX+1,posicaoGradeY-2);
			limparGrade(posicaoGradeX-1,posicaoGradeY+1);
			limparGrade(posicaoGradeX,posicaoGradeY+1);
			limparGrade(posicaoGradeX+1,posicaoGradeY+1);
		} else if (posicaoGradeY<Math.floor(posicaoY/320)) {
			console.log("Indo pra baixo...");
			gerarGrade(posicaoGradeX-1,posicaoGradeY+2);
			gerarGrade(posicaoGradeX,posicaoGradeY+2);
			gerarGrade(posicaoGradeX+1,posicaoGradeY+2);
			limparGrade(posicaoGradeX-1,posicaoGradeY-1);
			limparGrade(posicaoGradeX,posicaoGradeY-1);
			limparGrade(posicaoGradeX+1,posicaoGradeY-1);
		}
	}
	posicaoGradeX=Math.floor(posicaoX/320);
	posicaoGradeY=Math.floor(posicaoY/320);
}
virar=0;
andar=0;
document.addEventListener("keydown", function(event) {
	if (event.keyCode==37) {
		virar=1;
	}
	if (event.keyCode==38) {
		andar=1;
	}
	if (event.keyCode==39) {
		virar=-1;
	}
	if (event.keyCode==40) {
		andar=-1;
	}
	if (event.keyCode==84) {
		topDown=!topDown;
	}
});
document.addEventListener("keyup", function(event) {
	if (event.keyCode==37 || event.keyCode==39) {
		virar=0;
	}
	if (event.keyCode==38 || event.keyCode==40) {
		andar=0;
	}
});
//this.posX+=this.velocidade*Math.cos(this.direcao*Math.PI/180);
//this.posY+=this.velocidade*Math.sin(this.direcao*Math.PI/180);

setInterval(atualizarJogo,20);
window.onresize=atualizarTela;